<!DOCTYPE html>
<html lang="en" style="scroll-behavior: smooth;">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TIA BUSANA</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" 
integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{ url('assets/css/heroic-features.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('whatsapp')}}/floating-wpp.min.css">

    @yield('style')

</head>

<body>
<div id="myDiv" style=" z-index : 10 !important"></div>
@include('front.layouts.nav')

<!-- Page Content -->
<div class="container">


    @yield('content')
    <!-- Page Features -->
    <!-- /.row -->

</div>
<!-- /.container -->
<footer class="text-light bg-warning mb-n5" >
    @include('front.layouts.contact')
    <div class="container">
        <div class="row pt-5">
            <div class="col-12 text-center">
                <div class="container ">
                    <p class="font-weight-bold">
                    Tia Busana <i class="fa fa-copyright"></i> 2020
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top" style="position: fixed !important; right : 5% !important; bottom : 5% !important;">
    <i class="fa fa-chevron-up">
    </i>
</a>

<!-- Bootstrap core JavaScript -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('whatsapp')}}/floating-wpp.min.js"></script>
@yield('script')
<script type="text/javascript">
    $(function () {
      $('#myDiv').floatingWhatsApp({
        phone: '6287879470788',
        popupMessage: 'Dengan TIA BUSANA, jahit & vermak sesuai keinginanmu. Ada yang bisa kami bantu ? ',
        showPopup: true
      });
    });
  </script>
</body>

</html>
