<nav class="navbar navbar-expand-lg navbar-light bg-warning fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">TIA<span class="font-weight-bold">BUSANA</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('about')}}">Tentang</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('product')}}"><i class="fa fa-package"></i>
                        Layanan
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/cart"><i class="fa fa-shopping-cart"></i> Keranjang
                        @if (Cart::instance('default')->count() > 0)
                            <strong>
                                ({{ Cart::instance('default')->count() }})
                            </strong>
                        @endif
                    </a>
                </li>
                <li class="nav-item dropdown">
                    
                        <button type="button" btn-lg class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Masuk Akun</button>   
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                        @if (!auth()->check())
                            <a class="dropdown-item " href="{{  url('user/login') }}">Masuk</a>
                            <a class="dropdown-item" href="{{  url('user/register') }}">Daftar</a>
                        @else
                            <a class="dropdown-item" href="{{  url('user/profile') }}"><i class="fa fa-user"></i> Profil Anda</a>
                            <hr>
                            <a class="dropdown-item" href="{{  url('user/logout') }}"><i class="fa fa-lock"></i> Logout</a>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
