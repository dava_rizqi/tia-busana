<div class="footer-top">
    <div class="container">
        <div class="row pt-5">
            <div class="col-lg-6 col-sm-12 col-md-12 footer-newsletter">
                <h2> LOKASI KAMI </h2>
                <div class="gmap_canvas">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.335943540265!2d107.07563931476899!3d-6.2193547954978134!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698f47fc43de1b%3A0xea61d9f958b837d0!2sTia%20Busana!5e0!3m2!1sen!2sid!4v1593758289844!5m2!1sen!2sid" width="100%" height="400px"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 footer-contact">
                <div class="row">
                    <div class="col-12">
                        <h2> TIA BUSANA </h2>
                        <p> TIA BUSANA merupakan penyedia layanan jahit dan vermak online 
                            dengan model dan ukuran keinginan pelanggan dengan jaminan kualitas busana terbaik. 
                        </p>
                    </div>
                    <div class="col-12">

                            <h2> KONTAK </h2>
                            <p><strong>Bekasi Griya Asri 2</strong>
                            <br>
                            Jl.Kemuning Raya Blok J12
                            <br>
                            No.8, RT.006, RW.040
                            <br>
                            Kec. Tambun Selatan
                            <br>
                                Kab. Bekasi
                                <br>
                            <strong>Telepon:</strong>
                            +62 878-7947-0788
                            <br>
                        </p>
                    </div>
                </div>
            </div>
            <style>
                .gmap_canvas {
                    overflow: hidden;
                    position: relative;
                    background: non !important;
                    padding-bottom: 56.25%;
                }

                .gmap_canvas iframe {
                    left:0;
                    top:0;
                    height: 100%;
                    width: 100%;
                    position: absolute;
                }
            </style>
        </div>
    </div>
</div>
