@extends('front.layouts.master')

@section('content')
<div class="container">

    <h2 class="mt-5"><i class="fa fa-shopping-cart"></i> Keranjang Pemesanan</h2>
    <hr>

    @if ( Cart::instance('default')->count() > 0 )

        <h4 class="mt-5">{{ Cart::instance('default')->count() }} Item di keranjan pemesanan Anda</h4>

        <div class="cart-items">

            <div class="row">

                <div class="col-md-12">

                    @if ( session()->has('msg') )

                        <div class="alert alert-success">{{ session()->get('msg') }}</div>

                    @endif

                    @if ( session()->has('errors') )

                        <div class="alert alert-warning">{{ session()->get('errors') }}</div>

                    @endif

                    <table class="table">

                        <tbody>

                        @foreach (Cart::instance('default')->content() as $item )

                            <tr>
                                <td><img src="{{ url('/uploads') . '/'. $item->model->image }}" style="width: 5em"></td>
                                <td>
                                    <strong>{{ $item->model->name }}</strong><br> {{ $item->model->description }}
                                </td>

                                <td>

                                    <form action="{{ route('cart.destroy', $item->rowId) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-link">Hapus</button>
                                    </form>

                                    <form action="{{ route('cart.saveLater', $item->rowId) }}" method="post">

                                        @csrf

                                        <button type="submit" class="btn btn-link">Masukkan ke wishlist</button>

                                    </form>

                                </td>

                                <td>
                                    <select class="form-control quantity" style="width: 4.7em" data-id="{{ $item->rowId }}">
                                       @for ($i = 1; $i < 5 + 1; $i++)
                                        <option {{ $item->qty == $i ? 'selected' : '' }}>{{$i}}</option>
                                      @endfor

                                    </select>
                                </td>
                                @php
                                    $total = "Rp " . $item->total();
                                @endphp
                                <td>{{ $total }}</td>
                            </tr>
                        @endforeach


                        </tbody>

                    </table>

                </div>
                <!-- Price Details -->
                <div class="col-md-6">
                    <div class="sub-total">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th colspan="2">Detail Harga</th>
                            </tr>
                            </thead>
                            <tr>
                                @php
                                    $subtotal = "Rp " . Cart::subtotal();
                                    $tax = "Rp " . Cart::tax();
                                    $total = "Rp " . Cart::total();
                                @endphp
                                <td>Subtotal</td>
                                <td>{{ $subtotal }}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <th>{{$total }}</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- Save for later  -->
                <div class="col-md-12">
                    <a href="/" class="btn btn-outline-warning">Pilih layanan lagi</a>
                    <a href="/checkout" class="btn btn-outline-info">Lanjutkan Pemesanan</a>
                    <hr>
                </div>
                @else
                    <h3>Belum ada layanan yang anda pilih</h3>
                    <a href="/" class="btn btn-outline-warning">Pilih layanan lagi</a>
                    <hr>
                @endif


                @if ( Cart::instance('saveForLater')->count() > 0 )

                    <div class="col-md-12">

                        <h4>{{ Cart::instance('saveForLater')->count() }} Wishlist Anda</h4>
                        <table class="table">

                            <tbody>

                            @foreach (Cart::instance('saveForLater')->content() as $item )

                                <tr>
                                    <td><img src="{{ url('/uploads') . '/'. $item->model->image }}" style="width: 5em">
                                    </td>
                                    <td>
                                        <strong>{{ $item->model->name }}</strong><br> {{ $item->model->description }}
                                    </td>

                                    <td>

                                        <form action="{{ route('saveLater.destroy', $item->rowId) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-link">Hapus</button>
                                        </form>

                                        <form action="{{ route('moveToCart', $item->rowId) }}" method="post">

                                            @csrf

                                            <button type="submit" class="btn btn-link">Masukkan ke keranjang
                                            </button>

                                        </form>

                                    </td>

                                    <td>
                                        <select name="" id="" class="form-control" style="width: 4.7em">
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </td>
                                    @php
                                        $total = "Rp " . number_format( $item->total(),2,',','.');
                                    @endphp

                                    <td>{{ $total }}</td>
                                </tr>

                            @endforeach

                            </tbody>

                        </table>

                    </div>

                @endif
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script src="{{ asset('js/app.js') }}"></script>
    <script>

        const className = document.querySelectorAll('.quantity');

        Array.from(className).forEach(function (el) {
            el.addEventListener('change', function () {
                const id = el.getAttribute('data-id');
                axios.patch(`/cart/update/${id}`, {
                    quantity: this.value
                })
                    .then(function (response) {
//                        console.log(response);
                          location.reload();
                    })

                    .catch(function (error) {
                        console.log(error);
                        location.reload();
                    });
            });
        });


    </script>
@endsection