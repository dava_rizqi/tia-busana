@extends('front.layouts.master')

@section('content')

    <h2>Profil</h2>
    <hr>
    
    @if (session()->has('msg'))
    <div class="alert alert-success my-3">
        {{ session()->get('msg') }}
    </div>
    @endif

    <form action="{{route('update-profile')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $user->id }}">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2">Detail Pengguna </th>
            </tr>
            </thead>
            <tr>
                <th>ID</th>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <th>Nama</th>
                <td><input type="text" name="name" value="{{ $user->name}}" class="form-control"></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><input type="text" name="email" value="{{ $user->email}}" class="form-control"></td>
            </tr>
            <tr>
                <th>Nomor Telepon</th>
                <td><input type="number" name="phone_number" value="{{ $user->phone_number}}" class="form-control"></td>
            </tr>
            <tr>
                <th>Alamat Lengkap</th>
                <td>
                    <div class="d-flex">
                        <textarea name="address" class="form-control">{{ $user->address}}</textarea>
                    </div>
                </td>
            </tr>
            <tr>
                <th>Registered At</th>
                <td>{{ $user->created_at}}</td>
            </tr>
        </table>

        <button class="btn btn-success btn-lg my-3" type="submit">Ubah Profil</button>
    </form>
    

@endsection