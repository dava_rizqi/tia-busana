@extends('front.layouts.master')

@section('content')
<h2>Halaman Detail Pemesanan</h2>
<hr>


<div class="row">

    <div class="col-md-12">
        <h4 class="title"></h4>
        <div class="content table-responsive table-full-width">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th colspan="7">Detail Pemesanan</th>
                    </tr>
                    <tr>
                        <th>ID</th>
                        <th>Tanggal</th>
                        <th>Catatan</th>
                        <th>Status</th>
                        <th>Bukti Pembayaran</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->date }}</td>
                        <td>@if ($order->note)
                            <?= $order->note ?>
                        @endif</td>
                        <td>
                            @if ($order->status)
                            <span class="badge badge-success">Sudah dikonfirmasi</span>
                            @else
                            <span class="badge badge-warning">Pending</span>
                            @endif
                        </td>
                        @if($order->payment == NULL)
                        <td>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload
                                Bukti Pembayaran</a>
                        </td>
                        @else
                        <td><a href="{{asset('uploads')}}/{{ $order->payment }}" target="_blank">Lihat</td>
                        @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">

        <h4 class="title">Detail Pengguna</h4>
        <hr>
        <div class="content table-responsive table-full-width">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <td>{{ $order->user->id }}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{ $order->user->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $order->user->email }}</td>
                    </tr>
                    <tr>
                        <th>Nomor Telepon</th>
                        <td>{{ $order->user->phone_number }}</td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>{{ $order->user->address }}</td>
                    </tr>
                    <tr>
                        <th>Registered At</th>
                        <td>{{ $order->user->created_at->diffForHumans() }}</td>
                    </tr>

                </thead>
            </table>
        </div>
    </div>
    <div class="col-md-6">

        <h4 class="title">Detail Produk</h4>
        <hr>
        <div class="content table-responsive table-full-width">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Order ID</th>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                    <th>Kuantitas</th>
                    <th>Gambar</th>
                </tr>
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>
                        @foreach ($order->products as $product)
                        <table class="table">
                            <tr>
                                <td>{{ $product->name }}</td>
                            </tr>
                        </table>
                        @endforeach
                    </td>

                    <td>
                        @foreach ($order->orderItems as $item)
                        <table class="table">
                            <tr>
                                <td>{{ $item->price }}</td>
                            </tr>
                        </table>
                        @endforeach
                    </td>

                    <td>
                        @foreach ($order->orderItems as $item)
                        <table class="table">
                            <tr>
                                <td>{{ $item->quantity }}</td>
                            </tr>
                        </table>
                        @endforeach
                    </td>

                    <td>
                        @foreach ($order->products as $product)
                        <table class="table">
                            <tr>
                                <td><img src="{{ url('uploads') . '/' . $product->image }}" alt="" style="width: 2em">
                                </td>
                            </tr>
                        </table>
                        @endforeach
                    </td>
                </tr>

            </table>

        </div>
    </div>
</div>
<!-- Modal -->
<div id="uploadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @php
                    $total = 0;
                    foreach($order->products as $item){
                        $total += $item->price;
                    }
                @endphp
                <small>Total Belanja : </small>
                <h2>
                    {{"Rp " . number_format($total,2,',','.')}}
                </h2>
                <small>Kirim Ke no rek :</small>
                <h4>0183019289048 (BCA) a/n lorem</h4>
                <!-- Form -->
                <form method="POST" action="{{url('/user/order/verify')}}/{{$order->id}}" enctype="multipart/form-data">
                    {{ csrf_field()}}
                    Select file : <input type='file' name='payment' id='file' class='form-control'><br><br>
                    <button type="submit" class="btn btn-rounded btn-success"
                        style="position:fixed; bottom:20px; right:20px; z-index:1;">Upload
                    </button>
                </form>

                <!-- Preview-->
                <div id='preview'></div>
            </div>

        </div>

    </div>
</div>
@endsection