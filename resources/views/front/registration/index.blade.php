@extends('front.layouts.master')

@section('content')

    <div class="row justify-content-center">

    <div class="col-md-8" id="register">

        <div class="card ">
            <div class="card-body">
                <h2 class="card-title">Daftar</h2>
                <hr>

                @if ( $errors->any() )

                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif

                <form action="/user/register" method="post">

                    @csrf
                    <div class="row">
                        <div class="form-group col-sm-12 col-lg-6">
                            <label for="name">Nama :</label>
                            <input type="text" name="name" placeholder="Name" id="name" class="form-control">
                        </div>

                        <div class="form-group col-sm-12 col-lg-6">
                            <label for="email">Email :</label>
                            <input type="text" name="email" placeholder="Alamat Email" id="email" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address">Alamat Lengkap :</label>
                        <textarea name="address" placeholder="Alamat Lengkap" id="address" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="email">Nomor Telepon :</label>
                        <input type="type" name="phone_number" placeholder="Nomor Telepon" id="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password">Password :</label>
                        <input type="password" name="password" placeholder="Password" id="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Konfirmasi Password :</label>
                        <input type="password" name="password_confirmation" placeholder="Confirm Password" id="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-outline-info col-md-2"> Daftar</button>
                    </div>

                </form>

            </div>
        </div>

    </div>

</div>

@endsection
