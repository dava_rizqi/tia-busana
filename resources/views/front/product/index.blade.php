@extends('front.layouts.master')

@section('content')
    <div class="container">
    <!-- Jumbotron Header -->
        @if ( session()->has('msg') )
            <div class="alert alert-success">{{ session()->get('msg') }}</div>
        @endif


        <div class="row " id="catalogue">
        
            <div class="col-lg-12 col-sm-12 my-3">
                <div class="row">
                    @foreach ($categories as $category)
                    <div class="col-lg-4 col-sm-12 text-center my-1" id="category">
                        <a href="{{route('filter' , ['id' => $category->id])}}" style="text-decoration: none !important; color : white !important; cursor : pointer !important" for="submit">
                                <div class="card" style="position : relative !important ;background: url('{{asset('uploads/' . $category->image)}}'); background-size : cover !important; min-height : 180px !important; min-width : 350px !important">
                                    <div class="card-body">
                                        <div style="position: relative; z-index : 1 !important;">
                                            <h5 class="font-weight-bold">{{$category->name}}</h5>
                                                <p>{{$category->description}}</p>
                                            </div>
                                        </div>
                                        <div class="overlay"></div>
                                    </div>
                                </a>
                        </div>
                        @endforeach
                    </div>
                <div class="row my-4">
                    <div class="col-lg-9 col-sm-12" >
                        <h2>Layanan Kami</h2>
                        <small>Menampilkan {{count($products)}} Produk</small>
                    </div>
                    <div class="col-lg-3 col-sm-12">
                        <form action="{{route('search')}}" method="post">
                            @csrf
                            <div class="d-flex justify-content-between">
                                <input style="border-radius: 0px !important" type="text" name="keyword" placeholder="Cari sesuatu" class="form-control">
                                <button style="border-radius: 0px !important" type="submit" class="btn btn-outline-secondary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center">
                    <style>
                        .overlay { 
                            width: 100%;
                            height : 100%;
                            position: absolute;
                            z-index : 0 !important;
                            top : 0%;
                            left : 0%;
                            transition : 0.3s;
                            background: rgba(0, 0, 0, 0.452) !important;
                        }
                        #category:hover .overlay { 
                            background: black !important;
                        }
                    </style>
                   
                    </div>
            </div>
            @if (count($products) > 0)
                @foreach ($products as $product)
                <div class="col-lg-3 text-center col-md-6 mb-4">
                    <div class="card">
                        <img style="min-height: 300px; max-height: 300px" class="card-img-top" src="{{ url('/uploads') . '/' . $product->image }}" alt="">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text">
                            {{ $product->description }}
                            </p>
                        </div>
                        <div class="card-footer bg-warning text-light">
                            @php
                                $hasil_rupiah = "Rp " . number_format($product->price,2,',','.');
                            @endphp
                            <strong>{{ $hasil_rupiah }}</strong> &nbsp;
                            <form action="{{ route('cart') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="name" value="{{ $product->name }}">
                                <input type="hidden" name="price" value="{{ $product->price }}">
                            <button type="submit" class="btn btn-outline-light mt-3"><i class="fa fa-cart-plus "></i> Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                <div class="col-12 text-center my-5">
                    <h2>Tidak ada produk</h2>  
                    <a href="{{route('product')}}">Tampilkan semua</a>              
                </div>
            @endif
        </div>
    </div>

@endsection