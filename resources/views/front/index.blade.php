@extends('front.layouts.master')

@section('content')
    <div class="container">
    <!-- Jumbotron Header -->
        <header class="jumbotron my-4">
            <h5 class="display-3"><strong>Selamat Datang,</strong></h5>
            <p class="display-4"><strong>Mari di Order</strong></p>
            <p class="display-4">&nbsp;</p>
            <a href="#catalogue" class="btn btn-warning btn-lg float-right">Belanja Sekarang!</a>
        </header>
        @if ( session()->has('msg') )
            <div class="alert alert-success">{{ session()->get('msg') }}</div>
        @endif


        <div class="row" id="catalogue">
        
            <div class="col-lg-12 col-sm-12 my-3 d-flex justify-content-between">
                <h2>Layanan Kami</h2>
            </div>
        @foreach ($products as $product)
            <div class="col-lg-3 text-center col-md-6 mb-4">
                <div class="card" id="catalogue">
                    <img style="min-height: 300px; max-height: 300px" class="card-img-top" src="{{ url('/uploads') . '/' . $product->image }}" alt="">
                    <div class="card-body" >
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <p class="card-text">
                        {{ $product->description }}
                        </p>
                    </div>
                    <div class="card-footer bg-warning text-light">
                        @php
                            $hasil_rupiah = "Rp " . number_format($product->price,2,',','.');
                        @endphp
                        <strong>{{ $hasil_rupiah }}</strong> &nbsp;
                        <form action="{{ route('cart') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <input type="hidden" name="name" value="{{ $product->name }}">
                            <input type="hidden" name="price" value="{{ $product->price }}">
                            <button style="position : relative !important; z-index : 2 !important" type="submit" class="btn btn-outline-light mt-3"><i class="fa fa-cart-plus "></i> Tambahkan</button>
                        </form>
                    </div>
                </div>
            </div>

            @endforeach

            <div class="col-lg-12 col-sm-12 my-3 text-center">
                <a href="{{route('product')}}" class="btn btn-outline-warning mb-3">Lihat Semua layanan</a>
            </div>
        </div>
    </div>

@endsection