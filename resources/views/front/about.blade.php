@extends('front.layouts.master')

@section('content')
    
<div class="about" id="about">
    <div class="container">
        <h3>Tentang</h3>

        <div class="col-md-4 hhh">
            <div class="wthree_rt">
            <!-- <i class='fas fa-money-bill-wave-alt'></i> -->
                <img src="{{asset('vendor/frontend')}}/images/svc_money.png" alt="">
                <h4>Get More, Pay Less</h4>
                <p>Harga yang terjangkau untuk kualitas layanan premium.</p>
            </div>
        </div>
        <div class="col-md-4 hhh">
            <div class="wthree_rt">
                <!-- <i class="fa fa-space-shuttle" aria-hidden="true"></i> -->
                <img src="{{asset('vendor/frontend')}}/images/svc_varied.png" alt="">
                <h4>Varied Services</h4>
                <p>Jenis layanan bervariasi untuk setiap kebutuhan anda.</p>
            </div>
        </div>
        <div class="col-md-4 hhh">
            <div class="wthree_rt">
                <img src="{{asset('vendor/frontend')}}/images/svc_click.png" alt="">
                <h4>One Click Away</h4>
                <p><em>Website service available</em>,</p>
                <p>pesan laundry bisa dari rumah!</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="w3l_serdwn">
            <div class="col-md-5 hhh">
                <div class="agile_ser">
                    <img src="{{asset('vendor/frontend')}}/images/svc_deli.png" alt="">
                    <h4>Pick Up & Delivery Services Available</h4>
                    <p>Tersedia layanan antar-jemput paket laundry yang terjamin keamanannya.</p>
                </div>
            </div>
            <div class="col-md-7 W3ls_serv">
                <div class="agile_ser allCenter">
                    <div class="col-md-6" style="top: 25%;">
                        <p><strong>Book now!</strong> We pick it up, We clean it up, We deliver it Squeaky-Clean right
                            to your doorstep!</p>
                    </div>
                    <div class="col-md-6" style="top: 10%; left: 8%;">
                        <img src="{{asset('vendor/frontend')}}/images/abt_book.png" width="65%"
                            style="vertical-align:middle" class="img-responsive" alt="">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<!-- <a target="_blank" href="https://icons8.com/icons/set/diversity">Diversity icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a> -->
@endsection