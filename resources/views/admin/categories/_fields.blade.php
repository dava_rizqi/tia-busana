<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('category_name', 'Nama Kategori') }}
    {{ Form::text('name',$category->name,['class'=>'form-control border-input','placeholder'=>'Permak']) }}
    <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    {{ Form::label('description', 'Deskripsi ') }}
    {{ Form::textarea('description',$category->description,['class'=>'form-control border-input','placeholder'=>'Deskripsi singkat mengenai kategori']) }}
    <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
    {{ Form::label('file','Gambar Thumbnail') }}
    {{ Form::file('image', ['class'=>'form-control border-input', 'id' => 'image']) }}
    <div id="thumb-output"></div>
    <span class="text-danger">{{ $errors->has('image') ? $errors->first('description') : '' }}</span>
</div>