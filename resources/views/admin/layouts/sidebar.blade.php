<div class="sidebar" data-background-color="white" data-active-color="danger">

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                TIA<span class="font-weight-bold">BUSANA</span>
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{ url('/admin') }}">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/categories/create') }}">
                    <i class="ti-archive"></i>
                    <p>Tambah Kategori</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/categories') }}">
                    <i class="ti-view-list-alt"></i>
                    <p>Lihat Kategori</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products/create') }}">
                    <i class="ti-archive"></i>
                    <p>Tambah Layanan</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products') }}">
                    <i class="ti-view-list-alt"></i>
                    <p>Lihat Layanan</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/orders') }}">
                    <i class="ti-calendar"></i>
                    <p>Pemesanan</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/users') }}">
                    <i class="fa fa-users"></i>
                    <p>Pengguna</p>
                </a>
            </li>
        </ul>
    </div>
</div>
