<?php

use App\Category;
$categories = Category::pluck('name','id');

?>
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('product_name', 'Nama Layanan') }}
    {{ Form::text('name',$product->name,['class'=>'form-control border-input','placeholder'=>'Ex : Vermak baju  ']) }}
    <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>
</div>

<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
    {{ Form::label('price', 'Price') }}
    {{ Form::number('price',$product->price,['class'=>'form-control border-input','placeholder'=>'Rp 100.000,-']) }}
    <span class="text-danger">{{ $errors->has('price') ? $errors->first('price') : '' }}</span>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    {{ Form::label('description', 'Deskripsi') }}
    {{ Form::textarea('description',$product->description,['class'=>'form-control border-input','placeholder'=>'Deskripsi Layanan']) }}
    <span class="text-danger">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
    {{ Form::label('file','Gambar layanan') }}
    {{ Form::file('image', ['class'=>'form-control border-input', 'id' => 'image']) }}
    <div id="thumb-output"></div>
    <span class="text-danger">{{ $errors->has('image') ? $errors->first('description') : '' }}</span>
</div>
<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
    {{ Form::label('Category','Kategori ') }}
    {{ Form::select('category_id', $categories , ['class'=>'custom-select border-input', 'id' => 'Category']) }}
    <div id="thumb-output"></div>
    <span class="text-danger">{{ $errors->has('category_id') ? $errors->first('description') : '' }}</span>
</div>