<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserProfileController extends Controller
{
    public function index() {
        $id = auth()->user()->id;
        $user = User::where('id', $id)->first();

        return view('front.profile.index', compact('user'));
    }

    public function show($id) {
        $order = Order::find($id);
        return view('front.profile.details', compact('order'));
    }

    public function verifyPayment(Request $request, $id) {
        $order = Order::find($id);
        if ($request->hasFile('payment')) {
            $payment = $request->payment;
            $payment->move('uploads', $payment->getClientOriginalName());
            $order->payment = $request->payment->getClientOriginalName();
        }

        $order->update([
            'payment' => $order->payment
        ]);

        $request->session()->flash('msg', 'Wait for your payment to verified by Admin');
        return redirect('/user/profile');
    }

    public function edit($id){
        $id = auth()->user()->id;
        $user = User::where('id', $id)->first();

        return view('front.profile.edit', compact('user'));
    }
    
    public function update(Request $request){
        $id = $request->id;
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        $user->save();

        $request->session()->flash('msg', 'Your profile has been updated');
        return redirect()->back();
    }
}
