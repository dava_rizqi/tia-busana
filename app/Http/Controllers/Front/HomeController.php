<?php

namespace App\Http\Controllers\Front;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {

        $products = Product::inRandomOrder()->take(4)->get();

        return view('front.index', compact('products'));
    }
    public function about() {
        return view('front.about');
    }
    public function product() {

        $data['products'] = Product::all();
        $data['categories'] = Category::all();

        return view('front.product.index', $data);
    }
    public function search(Request $request) {
        // dd($request->filter);
        $data['categories'] = Category::all();
        $search = $request->keyword;
        $data['products'] = Product::where('name' , 'like' , '%' . $search . '%')->get();
        
        return view('front.product.index', $data);
    }
    public function filter($id) {
        // dd($request->filter);
        $data['categories'] = Category::all();
        $search = $id;
        $data['products'] = Product::where('category_id' , $search)->get();
        return view('front.product.index', $data);
    }
}
